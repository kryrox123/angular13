import { Component, OnInit } from '@angular/core';
import { Iformulario13 } from '../interfases/formulario13.interfaces';
@Component({
  selector: 'app-formulario13',
  templateUrl: './formulario13.component.html',
  styleUrls: ['./formulario13.component.css']
})
export class Formulario13Component implements OnInit {

    validar : Iformulario13 = {
    formularioCorreo : "" 
  };

  verificar : boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  guardar():void{ 
    console.log("correo verificado"); 
  }

}
